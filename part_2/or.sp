* Ehsan Jahangirzadeh 810194554
* ca1 question2 beta=2

**** Load libraries ****
.inc '32nm_bulk.pm'

**** Parameters ****
.param Lmin=32n
+VDD=1.1V

.global VDD

.temp   25

**** Source Voltage ****
VSource		vdd	 	 0	DC		VDD
VI1			vin1	 0	pulse	0   VDD   0n   10p   10p   50n   100n
VI2			vin2	 0	pulse	0   VDD   0n   10p   10p   25n   50n

**** Subcircuit invertor ****
.SUBCKT invertor vin vout
M2P 	vout     vin       vdd 	vdd  	pmos    w='4*Lmin'		L=Lmin
M1N 	vout     vin       0  	0    	nmos    w='2*Lmin'		L=Lmin
.ENDS invertor

**** Subcircuit nor ****
.SUBCKT nor vina vinb vout
M1N 	vout     vina       0   	0    nmos    w='2*Lmin'	L=Lmin
M2N 	vout     vinb       0   	0    nmos    w='2*Lmin'	L=Lmin

M3P 	m      	vina       vdd 	vdd  	pmos    w='8*Lmin'	L=Lmin
M4P 	vout    vinb       m 	m  		pmos    w='8*Lmin'	L=Lmin
.ENDS nor

**** or ****
anor 	vin1 	vin2 	nor_out 	nor
ainv		nor_out 	vout 	invertor

**** Analysis ****
.tran   1p 400n 1p

.end