* Ehsan Jahangirzadeh 810194554
* ca1 question1 beta=2

**** Load libraries ****
.inc '32nm_bulk.pm'

**** Parameters ****
.param Lmin=32n
+Beta=2
+VDD=1.1V

.temp   25

**** Source Voltage ****
VSource		vdd	 0	DC		VDD
VI			vin	 0	DC		0

**** Transistors ****
M2P vout     vin       vdd 	vdd  	pmos    w='2*Lmin*Beta'	L=Lmin
M1N vout     vin       0  	0    	nmos    w='2*Lmin'		L=Lmin

**** Analysis ****
.DC   VI  0   VDD  0.01

.end