* Ehsan Jahangirzadeh 810194554
* ca1 question1 beta=2 c=50

**** Load libraries ****
.inc '32nm_bulk.pm'

**** Parameters ****
.param Lmin=32n
+Beta=2
+CL=50ff
+VDD=1.1V

.temp   25

**** Source Voltage ****
VSource		vdd	 0	DC		VDD
VI			vin	 0	pulse	0	1	0n	10p		10p		20n		100n

**** Transistors ****
M2P 	vout     vin       vdd 	vdd  	pmos    w='2*Lmin*Beta'	L=Lmin
M1N 	vout     vin       0  	0    	nmos    w='2*Lmin'		L=Lmin

**** Capacitors ****
Cload 	vout 	 0 		   CL

**** Analysis ****
.tran   1p 400n 1p

.end